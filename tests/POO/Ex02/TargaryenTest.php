<?php

namespace App\Tests\POO\Ex02;

use App\Tests\SimplonTestCase;

final class TargaryenTest extends SimplonTestCase
{
    protected const DIR = __DIR__;

    public function testTargaryenClass(): void
    {
        include_once __DIR__ . '../../../../resources/Tests/testEx02.php';

        $this->expectOutputString(
            "Viserys burns alive\n"
            . "Daenerys emerges naked but unharmed\n"
        );
    }
}
