<?php

$input = $argv[1];
if (empty($input)) {
    exit;
}

const INVALID = "Wrong Format\n";

$formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::FULL);

const MONTHS = [
  1 => 'janvier',
  2 => 'fevrier',
  3 => 'mars',
  4 => 'avril',
  5 => 'mai',
  6 => 'juin',
  7 => 'juillet',
  8 => 'aout',
  9 => 'septembre',
  10 => 'octobre',
  11 => 'novembre',
  12 => 'decembre',
];

$epur_input = strtolower(trim($input));

$regex = "/([A-Za-z]+) (\d{1,2}) ([A-Za-z]+) (\d{4}) (([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1})/";

if (preg_match_all($regex, $epur_input, $matches)) {
    $day_text = $matches[1][0];
    $day_number = +$matches[2][0];
    $month_text = $matches[3][0];
    $year = +$matches[4][0];
    $time = $matches[5][0];
    $month_number = in_array($month_text, MONTHS) ? array_search($month_text, MONTHS) : 0;

    $test_date_format = checkdate($month_number, $day_number, $year);

    if ($test_date_format) {
        $final_date = "$day_text $day_number $month_text $year à $time" . ' heure normale d’Europe centrale';
        $timestamp = $formatter->parse($final_date);
        echo ($timestamp) ? $timestamp . "\n" : INVALID;
    } else {
        echo INVALID;

        return;
    }
} else {
    echo INVALID;

    return;
}
