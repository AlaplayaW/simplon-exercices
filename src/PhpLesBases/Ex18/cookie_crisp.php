<?php

// Parameters
const ACTION = 'action';
const NAME = 'name';
const VALUE = 'value';

// Action value
const DEL_ACTION = 'del';
const GET_ACTION = 'get';
const SET_ACTION = 'set';

function isSetValidAction(array $parameters): bool
{
    return ($parameters[ACTION] ?? null) === SET_ACTION && !empty($parameters[NAME]) && !empty($parameters[VALUE]);
}

function isGetOrDelValidAction(array $parameters): bool
{
    return in_array(($parameters[ACTION] ?? null), [DEL_ACTION, GET_ACTION]) && !empty($parameters[NAME]);
}

if (!count($_GET) || (!isSetValidAction($_GET) && !isGetOrDelValidAction($_GET))) {
    exit;
}

switch ($_GET[ACTION]) {
    case GET_ACTION:
        if (!empty($_COOKIE[$_GET[NAME]])) {
            echo $_COOKIE[$_GET[NAME]] . "\n";
        }

        break;
    case SET_ACTION:
        setcookie($_GET[NAME], $_GET[VALUE], time() + 3600);
        break;
    case DEL_ACTION:
        setcookie($_GET[NAME], '', time() - 1);
        break;
    default:
        exit;
}

// if ($_GET['action'] == 'set') {
//     if ($_GET['name'] && $_GET['value']) {
//         setcookie($_GET['name'], $_GET['value'], time() + (86400 * 30));
//     }
// } elseif ($_GET['action'] == 'get') {
//     $value = $_COOKIE[$_GET['name']];
//     if (isset($value)) {
//         echo $value . "\n";
//     }
// } elseif ($_GET['action'] == 'del') {
//     setcookie($_GET['name'], '', time() - 3600);
// }

// Solution de Yoan
// foreach ($_GET as $key => $value) {
//   if ($key == 'action') {
//       switch ($value) {
//           case 'set':
//               setcookie($_GET['name'], $_GET['value'], time() + 3600);
//               break;
//           case 'get':
//               if (isset($_COOKIE[$_GET['name']])) {
//                   echo $_COOKIE[$_GET['name']] . "\n";
//               }
//               break;
//           case 'del':
//               setcookie($_GET['name'], $_GET['value'], time() - 3600);
//               break;
//       }
//   }
// }
