<?php

if (!file_exists($argv[1])) {
    exit;
}

$path_parts = pathinfo($argv[1]);
$filename_input = $path_parts['basename'];
// $filename_input = 'file1.html';
$filename_output = 'new_page.html';
$qualifiedName = 'title';

$html = file_get_contents($filename_input, true, null, 0);

$reg3 = "#<a.*?(?:title=[\"'](.*)[\"'])?>(.*?)<(?:\/a>|(?:.*(?:title=[\"'](.*)[\"'])))#";

$new_html = preg_replace_callback($reg3, function ($m) {
    foreach (array_slice($m, 1) as $value) {
        $test = str_replace($value, strtoupper($value), $m[0]);
        $m[0] = $test;
    }

    return $m[0];
}, $html);

echo $new_html;
// file_put_contents($filename_output, $new_html);
