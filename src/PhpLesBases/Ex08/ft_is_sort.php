<?php

function ft_is_sort(array $tab): bool
{
    $sortedtab = $tab;
    sort($sortedtab);

    return $tab === $sortedtab;
}
