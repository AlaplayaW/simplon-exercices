<?php

echo 'Entrez un nombre: ';
// boucle infini
while (true) {
    // affichage du input
    $pile = trim(fgets(STDIN));

    // controle si le nombre est un numerique
    if (is_numeric($pile)) {
        // controle si le nombre est pair ou impaire
        if ($pile % 2 == 0) {
            echo "Le chiffre $pile est Pair\nEntrez un nombre: ";
        } else {
            echo "Le chiffre $pile est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'$pile' n'est pas un chiffre\nEntrez un nombre: ";
    }
}

// while (1) {

//     echo 'Entrez un nombre: ';
//     $number = rtrim(fgets(STDIN));
//     if (is_numeric(($number))) {
//         if (!(abs(fmod($number, 2)))) {
//             echo "Le chiffre $number est Pair\n";
//         } else {
//             echo "Le chiffre $number est Impair\n";
//         }
//     } elseif ($number === null) {
//         echo "\n";
//         exit;
//     }
//      else {
//         echo "'$number'" . " n'est pas un chiffre\n";
//     }
// }
