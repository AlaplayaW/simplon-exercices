<?php

const SYNTAX_ERROR = "Syntax Error\n";
const INCORRECT_PARAMETERS = "Incorrect Parameters\n";

function calcul($x, $operator, $y)
{
    switch ($operator) {
        case '+':
            return $x + $y;
            break;
        case '-':
            return $x - $y;
            break;
        case '*':
            return $x * $y;
            break;
        case '/':
            return $x == 0 || $y == 0 ? 0 : ($x / $y);
            break;
        case '%':
            return $x = 0 ? SYNTAX_ERROR : abs(fmod($x, $y));
            break;
    }
}

if ($argc != 2) {
    echo INCORRECT_PARAMETERS;
    exit;
}

// Déclaration des variables utilisées.

// Déclaration des patterns Regex.
$super_regex = "[(-?\d*\.{0,1}\d+)([\*|\+|\-|\/|\%])((?1))]";
$alpha = '/[A-Za-z]+/';

// On supprime les espaces.
$epur = preg_replace("/\s+/", '', trim($argv[1]));

if (preg_match($super_regex, $epur, $match) && !preg_match($alpha, $epur, $match_alpha)) {
    $number1 = +$match[1];
    $number2 = +$match[3];
    $op = $match[2];

    echo calcul($number1, $op, $number2) . "\n";
} else {
    echo SYNTAX_ERROR;
}
