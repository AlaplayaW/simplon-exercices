<?php

// $the_exploded = [];
// $params = [];
// $magic_key = $argv[1];

// foreach (array_slice($argv, 2) as $value) {
//     if (preg_match('/.+:.+/', $value, $match)) {
//         $the_exploded = explode(':', $value);
//         $params[$the_exploded[0]] = $the_exploded[1];
//     }
// }

// if (isset($params[$magic_key])) {
//     echo $params[$magic_key] . "\n";
// }

if ($argc < 3) {
    return;
}

$find = '';

foreach (array_slice($argv, 2) as $keyValue) {
    if (!preg_match("/^\S+:\S+$/", $keyValue)) {
        continue;
    }

    [$key, $value] = explode(':', $keyValue);

    if ($key === $argv[1]) {
        $find = $value;
    }
}

if (!empty($find)) {
    echo "$find\n";
}
