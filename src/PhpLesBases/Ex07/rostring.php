<?php

if ($argc < 2) {
    exit;
}

$firstchain = $argv[1];

$epur = preg_replace("/\s+/", ' ', trim($firstchain));

// Transforme une chaine de caractère en tableau
$array = explode(' ', $epur);

// Supprime le 1er element du tableau, et le stocke dans une variable
$firstword = array_shift($array);

// Ajoute le nouvel element à la fin du tableau
$array[] = $firstword;

// Transforme un tableau en string
print_r(implode(' ', $array) . "\n");
