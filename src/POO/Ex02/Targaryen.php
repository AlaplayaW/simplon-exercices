<?php

namespace App\POO\Ex02;

class Targaryen
{
    public const FIREPROOF = 'emerges naked but unharmed';
    public const BURN = 'burns alive';

    public function resistsFire(): bool
    {
        return false;
    }

    public function getBurned()
    {
        return $this->resistsFire() ? static::FIREPROOF : static::BURN;
    }
}

// self : la classe où ce mot-clé est écrit (classe “courante”)
// static : la classe appelée pendant l’exécution (class “active”)

// Methode de Yohan : method_exist()
// class Targaryen
// {
//     public const FIREPROOF = 'emerges naked but unharmed';
//     public const BURN = 'burns alive';

//     public function getBurned()
//     {
//         return method_exists($this, 'resistsFire') ? static::FIREPROOF : static::BURN;
//     }
// }
