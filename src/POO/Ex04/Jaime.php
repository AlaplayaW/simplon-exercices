<?php

namespace App\POO\Ex04;

use App\Resources\Classes\Lannister\Lannister;
use ReflectionClass;

// You can do this with reflection. Specifically, you can use the ReflectionClass::getShortName method, which gets the name of the class without its namespace.

class Jaime extends Lannister
{
    public const BIRTH_ANNOUNCEMENT = "My name is Jaime\n";

    public const OH_YES = "Let's do this.\n";
    public const OH_NO = "Not even if I'm drunk !\n";
    public const WHY_NOT = "With pleasure, but only in a tower in Winterfell, then.\n";

    protected function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT;
            echo self::BIRTH_ANNOUNCEMENT;
        }
    }

    public function sleepWith($someone): void
    {
        $reflect = new ReflectionClass($someone);
        $className = $reflect->getShortName();
        switch ($className) {
            case 'Tyrion':
                $result = self::OH_NO;
                break;
            case 'Sansa':
                $result = self::OH_YES;
                break;
            case 'Cersei':
                $result = self::WHY_NOT;
                break;
            default:
                $result = self::OH_NO;
                break;
        }
        echo $result;
    }

    // public function sleepWith($partenaire) {

    //     // Si le partenaire est Tyrion, alors on affiche le message drunk
    //     if ($partenaire instanceof Tyrion) {
    //         echo self::DRUNK;
    //     // Si le partenaire est Sansa, alors on affiche le message do
    //     } elseif ($partenaire instanceof Sansa) {
    //         echo self::DO;
    //     // Si le partenaire est Cersei, alors on affiche le message pleasure
    //     } elseif ($partenaire instanceof Cersei) {
    //         echo self::PLEASURE;
    //     // Finalement on affiche une erreur si aucun partenaire correspond
    //     } else {
    //         echo "error \n";
    //     }
    // }
}
