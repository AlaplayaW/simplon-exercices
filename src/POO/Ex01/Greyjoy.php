<?php

namespace App\POO\Ex01;

class Greyjoy
{
    protected $familyMotto = 'We do not sow';
}

// class Test {
  // public $var1;
  // protected const TEST='test;
  // protected $test='test;
// }

// on ne peut pas accéder à l'attribut d'une classe sans l'avoir instanciée: $test = new Test(); $test->test;
// Pas besoin d'instancier une classe qui utilise une méthode statique.
// Si non statique, on instancie.

// Private : Les enfants et les classes qui extends Test n'ont pas accès
// Protected : Accessible aux enfants
// Public : Dispo partout dans l'appli
// Methode d'accès à la variable
// new Test()->var1
// Methode d'accès à une const
// si ce sont des const, self::CONST par ex.
// Test::TEST

// self et static uniquement pour les consts
