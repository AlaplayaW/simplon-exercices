<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    protected const ABSORB_SUCCES = '(Factory absorbed a fighter of type ';
    protected const ALREADY_ABSORBED = '(Factory already absorbed a fighter of type ';
    protected const ABSORB_FAIL = "(Factory can't absorb this, it's not a fighter";
    protected const NOT_ABSORBED = "(Factory hasn't absorbed any fighter of type ";
    protected const FABRICATE_SUCCESS = '(Factory fabricates a fighter of type ';
    protected $fighters_tab = [];

    public function absorb(object $fighter): object
    {
        $message = self::ABSORB_FAIL;

        if ($fighter instanceof Fighter) {
            $spe = $fighter->speciality;

            $message = key_exists($spe, $this->fighters_tab) ? self::ALREADY_ABSORBED : self::ABSORB_SUCCES;
            $message .= $spe;
            $this->fighters_tab[$spe] = $fighter;
        }

        echo $message . ')' . PHP_EOL;

        return $this;
    }

    public function fabricate(string $spe)
    {
        $message = key_exists($spe, $this->fighters_tab) ? self::FABRICATE_SUCCESS : self::NOT_ABSORBED;

        echo $message . $spe . ')' . PHP_EOL;

        foreach ($this->fighters_tab as $key => $value) {
            if ($spe == $key) {
                return new $value();
            }
        }
    }
}
