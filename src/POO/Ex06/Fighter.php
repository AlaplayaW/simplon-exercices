<?php

namespace App\POO\Ex06;

abstract class Fighter
{
    public $speciality;

    abstract protected function fight();

    protected function __construct($soldier)
    {
        return $this->speciality = $soldier;
    }
}
