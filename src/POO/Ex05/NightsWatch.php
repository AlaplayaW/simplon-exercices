<?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    private string $fight = '';

    public function recruit(object $someone): void
    {
        if ($someone instanceof IFighter) {
            $this->fight .= $someone->fight();
        }
        // $someone instanceof IFighter ? $someone->fight() : 0;
    }

    public function fight(string $target = ''): void
    {
    }
}
