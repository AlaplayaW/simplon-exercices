<?php

namespace App\Resources\Classes\Targaryen;

use App\POO\Ex02\Targaryen;

class Daenerys extends Targaryen
{
    public function resistsFire(): bool
    {
        return true;
    }
}
