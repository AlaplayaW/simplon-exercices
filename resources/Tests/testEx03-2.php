<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Resources\Classes\DrHouse;

$house = new DrHouse();
$house->introduce();
